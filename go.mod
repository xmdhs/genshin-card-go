module github.com/xmdhs/genshin-card

go 1.17

require (
	github.com/VictoriaMetrics/fastcache v1.8.0
	github.com/julienschmidt/httprouter v1.3.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
)
